import json
from collections import Counter
import matplotlib.pyplot as plt
import sys


def nackbox_finder():
    with open(sys.argv[1]) as f:
        sage_protocol = json.load(f)
        sage_protocol = sage_protocol['packets']

    audio_ack = []
    for packet in sage_protocol:
        if packet['packet_type'] == 8:
            # audio_ack.append("{0:b}".format(sage_protocol[i]['decoded_data']['AudioAckVector']))
            audio_ack.append(packet['decoded_data']['AudioAckVector'])
    missing_packets = 0
    missing_box = []
    for ack_packet in audio_ack:
        if ack_packet != 65535:
            j = 65535 - ack_packet
            missing_packets = missing_packets + j
            null_box = [i for i in range(ack_packet.bit_length()) if not ack_packet & (1 << i)]
            missing_box = missing_box + null_box
    missing_box = Counter(missing_box)

    plt.bar(range(len(missing_box)), missing_box.values(), align='center')
    plt.xticks(range(len(missing_box)), list(missing_box.keys()))
    plt.title('Nack boxes')
    plt.xlabel('box position')
    plt.show()


if __name__ == "__main__":
    nackbox_finder()