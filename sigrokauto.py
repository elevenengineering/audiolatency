import numpy as np
import matplotlib.pyplot as plt
import os
import sounddevice as sd
import time
from threading import Thread


class LatencyTest(object):

    def __init__(self, logger=open('AudioLatencyTest.log', 'w'), fs=48000, duration=2, period=12000):
        self.fs = fs
        self.duration = duration
        self.sample_num = self.fs * self.duration
        self.pulse_width = 4
        self.period = period
        self.logger = logger
        logger.write('*** Audio Latency Testing ***\n\n')

    def play_wave(self):
        sig = np.int8(np.arange(self.sample_num) % self.period < self.pulse_width)
        sd.play(sig, self.fs)

    @staticmethod
    def sample_audio():
        os.chdir('sigrok-cli')
        os.system('cmd /c sigrok-cli --driver fx2lafw --config samplerate=24m --continuous -P '
          'i2s:sd=D0:ws=D1:sck=D2 -P i2s:sd=D4:ws=D5:sck=D6 -A i2s=left > test.txt')

    def visualize(self):
        Txsignal = []
        Rxsignal = []
        with open('test.txt', 'rt') as fin:
            for line in fin:
                hex_num = line[21:27]
                value = int(hex_num, 16)
                if (value & 0x800000) == 0x800000:
                    value = -((value ^ 0xffffff) + 1)
                if line[4] == '1':
                    Txsignal.append(value)
                else:
                    Rxsignal.append(value)

        if len(Txsignal) != len(Rxsignal):  # keep signals in same length
            l = min(len(Txsignal), len(Rxsignal)) - 1
            Txsignal = Txsignal[5000:l]
            Rxsignal = Rxsignal[5000:l]

        Time = np.linspace(0, len(Rxsignal) / self.fs, num=len(Rxsignal))
        n = len(Time)

        Txsignal = Txsignal / np.linalg.norm(Txsignal)
        if np.isnan(sum(Txsignal)) or Txsignal is None:
            self.logger.write('\tcannot capture data from transmitter\n')

        Rxsignal = Rxsignal / np.linalg.norm(Rxsignal)
        if np.isnan(sum(Rxsignal)) or Rxsignal is None:
            self.logger.write('\tcannot capture data from receiver\n')
        # Rxsignal = Rxsignal + np.random.normal(0, 0.05, n)

        try:
            correlation = np.correlate(Rxsignal[::5], Txsignal[::5], 'same')
        except:
            raise SystemExit('cannot capture data from logic analyzer')
        delay_arr = np.linspace(-0.5*n/self.fs, 0.5*n/self.fs, n)
        delay_arr = delay_arr[::5]
        delay = delay_arr[np.argmax(correlation)]
        self.logger.write('\tAudio Latency = {}s'.format(delay))

        fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(15, 8))
        ax1.set_title("Test Wave")
        ax1.plot(Time, Txsignal, color='b', label='TX')
        ax1.plot(Time, Rxsignal, color='r', label="RX")
        ax1.legend()
        ax1.set_xlabel('seconds', fontsize=15)
        ax1.xaxis.set_label_coords(1, -0.025)
        ax2.set_title("Correlation")
        ax2.plot(delay_arr, correlation)
        plt.show()


if __name__ == '__main__':
    a = LatencyTest()
    thread = Thread(target=a.play_wave).start()
    time.sleep(0.5)
    a.sample_audio()
    a.visualize()