# AUDIO LATENCY

return latency and plot correlated output

## Requirements:

pip install sounddevice

## Usage

### correlation.py


Usage: python sigrokauto.py

## Hardware Setup

One Sage dev board (Rx)

One Thyme board (TX)

Logic analyzer connection:

TX: sd=D0,ws=D1,sck=D2; Rx: sd=D4,ws=D5,sck=D6